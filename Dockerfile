# use small node image
FROM node:alpine

# install git ca-certificates openssl openssh for CircleCI
# install jq for JSON parsing
RUN apk add --update --no-cache git openssh ca-certificates openssl jq gettext xmlstarlet curl

# install latest sfdx from npm
RUN node --version
RUN npm install sfdx-cli --global
RUN npm install --global vlocity
RUN sfdx --version
RUN vlocity help
RUN sfdx plugins --core

#salesforcedx plugin is now deprecated
#RUN sfdx plugins:install salesforcedx@latest-rc

# revert to low privilege user
USER node